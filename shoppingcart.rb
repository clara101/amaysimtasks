class ShoppingCart
  attr_reader :rules, :item_list, :promo_code

  def initialize(rules)
    @item_list = []
    @rules = rules
    @promo_code = nil
  end

  def add(args)
    item = args[:item]
    if args[:promo_code] != nil
      @promo_code = args[:promo_code]
    end
    @item_list.push(item)
  end

  def total
  	total = 0.0

  	@items.each do |item|
      total += item.price
      end
    #If Unlimited 1GB Sims > 3, you will pay the price of 2 only 
    if items1 = 3 
        return total - (total - 24.90)
    #Adding the promo code 'I<3AMAYSIM' will apply a 10% discount
    else @items.promo_code = "I<3AMAYSIM"
        return total - (total * 0.10)
    end
  end

  def items
    puts "Shopping Cart"
    puts "-" * 40

    puts "Item".ljust(30) + "Price".rjust(10)
    catalog = self.rules[:catalog]
    self.item_list.each do |item|
        puts catalog[item.to_sym][:name].ljust(30) + sprintf("%0.2f", catalog[item.to_sym][:price]).rjust(10)
    end

    puts "-" * 40
  #  puts "Total:".ljust(30) + sprintf("%0.2f", total).rjust(10)
    puts "-" * 40
  end

end

item1 = ["Unlimited 1GB", 24.90]
item2 = ["Unlimited 2GB", 29.90]
item3 = ["Unlimited 5GB", 44.90]
item4 = ["1 GB Data-pack", 9.90]
rules = {
    catalog: {
      'item1': {
        name: 'Unlimited 1GB',
        price: 24.90
      },
      'item2': {
        name: 'Unlimited 2GB',
        price: 29.90
      },
      'item3': {
        name: 'Unlimited 5GB',
        price: 44.90
      },
      'item4': {
        name: '1 GB Data-pack',
        price: 9.90
      }
    },
    promo_code: {
      'I<3AMAYSIM': 0.0
    }
  }

cart = ShoppingCart.new(rules)
cart.add(item: item1)
cart.add(item:item2, promo_code: promo_code)
puts cart.total
puts cart.items
