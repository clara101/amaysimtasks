require_relative "shoppingcart.rb"
require "test/unit"

# command to run this file: ruby test_shopping_cart.rb

class TestShoppingCart < Test::Unit::TestCase

  def setup
    rules = {
    catalog: {
      'item1': {
        name: 'Unlimited 1GB',
        price: 24.90
      },
      'item2': {
        name: 'Unlimited 2GB',
        price: 29.90
      },
      'item3': {
        name: 'Unlimited 5GB',
        price: 44.90
      },
      'item4': {
        name: '1 GB Data-pack',
        price: 9.90
      }
    },
    promo_code: {
      'I<3AMAYSIM': 0.0
    }
  }
    @cart = ShoppingCart.new(rules)
  end

  def test_creates_shopping_cart
    assert_kind_of(ShoppingCart,@cart, 'Cart was not created')
  end

  def test_cart_has_rules
    assert_equal(@cart.rules[:catalog][:item1][:name],'Unlimited 1GB','Cart has no pricing rules')
    assert_equal(@cart.rules[:catalog][:item2][:name],'Unlimited 2GB','Cart has no pricing rules')
    assert_equal(@cart.rules[:catalog][:item3][:name],'Unlimited 5GB','Cart has no pricing rules')
    assert_equal(@cart.rules[:catalog][:item4][:name],'1 GB Data-pack','Cart has no pricing rules')
    assert_equal(@cart.rules[:promos][:I<3AMAYSIM],0.0,'Cart has no pricing rules')
  end

  # def teardown
  # end

  # def test_fail
  #   assert(false, 'Assertion was false.')
  # end

end
